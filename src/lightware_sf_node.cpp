#include <stdio.h>
#include <string.h>
#include <sstream>
#include <ros/ros.h>
#include <std_msgs/ByteMultiArray.h>
#include <sensor_msgs/Range.h>
#include "boost/tokenizer.hpp"
#include "boost/lexical_cast.hpp"
#include <boost/algorithm/string.hpp>
#include <msgs/serial.h>
ros::Publisher range_pub;
sensor_msgs::Range range_msg;

typedef boost::tokenizer<boost::char_separator<char> > tokenizer;

void gps_parser(tokenizer& tokens, ros::Time time_recv)
{
	std::vector<std::string> data;
	try
	{
		data.assign(tokens.begin(), tokens.end());
		if(data.size() == 6)
		{
			if((data.at(1) == "m") and (data.at(3) == "V")) // CHeck if we got m and V in the string
			{
				ROS_DEBUG_STREAM("Dist " << data.at(0) << data.at(1)); // print the measured distance as debug info
				range_msg.header.stamp = time_recv;
				range_msg.range = boost::lexical_cast<double>(data.at(0));
				range_pub.publish(range_msg);
			}
	 }
	}catch (boost::bad_lexical_cast &){
		ROS_WARN("LightWare_laser_parser::bad lexical cast");
	}
}


void serialCallbackString(const msgs::serial::ConstPtr& msg)
{
	boost::char_separator<char> sep(" ");
	tokenizer::iterator tok_iter;
	tokenizer tokens(msg->data, sep);

	gps_parser(tokens, msg->header.stamp);

}

int main(int argc, char **argv)
{
	ros::init(argc, argv, "LightWare_laser_parser");
	ros::NodeHandle nh;
	ros::NodeHandle n("~");

	double sensor_min_range;
	double sensor_max_range;
	double sensor_field_of_view;
	std::string frame_id;
	std::string subscribe_serial_topic_id;
	std::string publish_topic_id;


  n.param<double>("sensor_min_range", sensor_min_range, 0.1);
	n.param<double>("sensor_max_range", sensor_max_range, 120.0);
	n.param<double>("sensor_field_of_view", sensor_field_of_view, 0.0077);
	n.param<std::string>("frame_id", frame_id,"laser_link");
	n.param<std::string>("serial_topic_id", subscribe_serial_topic_id,"/fmData/rx");
	n.param<std::string>("publish_range_id", publish_topic_id,"fmInformation/range_data");


  range_msg.header.frame_id = frame_id;
	range_msg.radiation_type = sensor_msgs::Range::INFRARED;
	range_msg.min_range = sensor_min_range;
	range_msg.max_range = sensor_max_range;
	range_msg.field_of_view = sensor_field_of_view;

	ros::Subscriber sub = n.subscribe(subscribe_serial_topic_id, 10, serialCallbackString);
	range_pub = n.advertise<sensor_msgs::Range>(publish_topic_id, 10);

	ros::spin();
	return 0;
}
