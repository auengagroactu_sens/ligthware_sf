# Lightware SF series ROS node
A ROS driver for the lightware SF series.
The driver can be used together with both usb-serial and direct serial interface pressent on the lightware units.
The node uses the [sensor_msgs/Range](http://docs.ros.org/api/sensor_msgs/html/msg/Range.html) message to output the measured distance.
Currently we have tested the ROS node with the LightWare SF11:

![LightWare SF11 image](https://pbs.twimg.com/media/CdyE9a_WoAEEVe3.jpg "LightWare SF11")

But we expect it to work with other units in the SF series from Lightware.

The node utilizes the  [serial_string](https://github.com/FroboLab/frobomind/tree/master/fmLib/computer_support/serial_string) ROS node for recieve serial data.
The serial_string is part of the [Frobomind](http://frobomind.org/web/doku.php) robot control system software platform.
If you do not what to check out all the frobomind nodes, a link with a few basic nodes from FroboMind repo can be found here:
